$(document).ready(function() {
  $('#tabContainer').tabs({
		show: 'fadeIn',
		hide: 'fadeOut'
  });

	$('.panel4').hide();

	$.getJSON('data.json',function(data){

		var summerHTML = '',
			summerHTML1 = '', 
			summerHTML2 = ''; 
		
		$.each(data.first,function(i,summer){
		summerHTML +='<div id="lista"><p>Liczba porządkowa: ' + summer.id + '<span class="delete">x</span></p>';
		summerHTML +='<p>Państwo: ' + summer.countryName + '</p>';
		summerHTML +='<p>Region: ' + (summer.regionName || "-") + '</p>';
		summerHTML +='<p>Nazwa hotelu: ' + summer.name + '</p><hr></div>'
		
		});
		
		$.each(data.second,function(i,summer1){
		summerHTML1 +='<div id="lista"><p>Liczba porządkowa: ' + summer1.id + '<span class="delete">x</span></p>';
		summerHTML1 +='<p>Państwo: ' + summer1.countryName + '</p>';
		summerHTML1 +='<p>Region: ' + (summer1.regionName || "-") + '</p>';
		summerHTML1 +='<p>Nazwa hotelu: ' + summer1.name + '</p><hr></div>'
		
		});
		
		$.each(data.third,function(i,summer2){
		summerHTML2 +='<div id="lista"><p>Liczba porządkowa: ' + summer2.id + '<span class="delete">x</span></p>';
		summerHTML2 +='<p>Państwo: ' + summer2.countryName + '</p>';
		summerHTML2 +='<p>Region: ' + (summer2.regionName || "-") + '</p>';
		summerHTML2 +='<p>Nazwa hotelu: ' + summer2.name + '</p><hr></div>'
		
		});
		
		
		$('#panel1').append(summerHTML);
		$('#panel2').append(summerHTML1);
		$('#panel3').append(summerHTML2);
		
	});



	$(".panel").on('click','.delete',function(){
		
			var $taskItem = $(this).closest('div');
			$taskItem.slideUp(250,function(){
				var $this = $(this);
				$this.detach();
				$('.panel4').show();
				$('#panel4').prepend($this);
				$this.slideDown();
			});
		});


		$('#range').on('change',function(){

			
			var $val = $('#range').val();
			var $tab = $('#tabContainer');
			
			if ($val == 0) {
				$tab.addClass('rotation').removeClass('rotation1');
			}else if($val == 2) {
				$tab.toggleClass('rotation1');

			}else{
				$tab.removeClass('rotation rotation1');
			}
		})
	
}); // Koniec funkcji ready.