function GraOTron() {
  var domBoard = document.getElementById('board');
  var myTrace = [];
  var myVec = 'right';
  var speed = 500;
  var boardWidth = 30;
  var boardHeight = 30;
  var score = 0;

  function generateBoard(width, height) {
    var iY, domTR,
        iX, domTD,
	    domTextAddr,
	    domTBODY;
	  
    domTBODY = document.createElement('tbody');
 
    for(iY=0; iY<height; iY++){
      domTR = document.createElement('tr');
      domTBODY.appendChild(domTR);
	
	  for(iX=0; iX<width; iX++) {
		domTD = document.createElement('td');
		domTR.appendChild(domTD);
		
		domTextAddr = document.createTextNode(iX+'x'+iY);
		//domTD.appendChild(domTextAddr);
		
		domTD.id = 'addr'+iX+'x'+iY;
		domTD.className = 'pole';
  	  }
    }

    domBoard.appendChild(domTBODY);  
  }

  function point(x, y, color) { 
    document.querySelector('#addr'+x+'x'+y).style.backgroundColor = color;
  }

  function initTrace() {
    var initLength = 5,
        i;

    for (i=0; i<initLength; i++) {
      myTrace.push({x: i, y:0});
	}

	myTrace.forEach(function(tracePoint) {
	  point(tracePoint.x, tracePoint.y, 'black');
	});
  }

  function validateTrace() {
    var head = myTrace[myTrace.length-1];
    var i;
	
	if ((head.x>=boardWidth) || (head.x<0)) return false;
	if ((head.y>=boardHeight) || (head.y<0)) return false;
		
	for (i=0; i<myTrace.length-2; i++) {
		if ((myTrace[i].x == head.x) && (myTrace[i].y == head.y)) return false;
	}
	
	return true;
  }  
  
  function addScore() {
    score++; // score=score+1;
	
	if (!(score % 10)) {
	  speed = speed - 100;
	}
	
	document.querySelector('#score').innerHTML = score;
  }
  
  function play() {
    var tail;
	var head;
	
	// ucinamy ogon
	if (score % 5) {
	  tail = myTrace.shift();
      point(tail.x, tail.y, 'transparent');
	}

	// dodajemy glowe
	head = myTrace[myTrace.length-1];
	switch(myVec) {
      case 'right':
	    myTrace.push({x: head.x+1, y: head.y});
		break;
      case 'left':
	    myTrace.push({x: head.x-1, y: head.y});
		break;
      case 'up':
	    myTrace.push({x: head.x, y: head.y-1});
		break;
      case 'down':
	    myTrace.push({x: head.x, y: head.y+1});
		break;
	}
	head = myTrace[myTrace.length-1];
	
	if (validateTrace()) {
      point(head.x, head.y, 'black');
	  addScore();
	  setTimeout(play, speed);
	} else {
		alert('przegrales!');
	}
  }

  function send(method, url, data, cbk) {
    function buildQueryString(data) {
	  var props = Object.keys(data),
	      i,
		  tmpVariables = [];

	  for(i=0; i<props.length; i++) {
		tmpVariables.push(encodeURIComponent(props[i]) + '=' + encodeURIComponent(data[props[i]]));
	  }
	  
	  return tmpVariables.join('&');
	}

	function sendGET(url, data, cbk) {
	  var xhr = new XMLHttpRequest();
	  xhr.open('GET', url + '?' + buildQueryString(data), true); //adres i metoda true oznacza tu asynchronicznosc
	  xhr.onreadystatechange = function() {  //funkcja odebrania danych
	    if (4 == xhr.readyState && 200 == xhr.status) {
			cbk(JSON.parse(xhr.responseText));
		}
	  }
	  xhr.send();
    }

	function sendPOST(url, data, cbk) {
	  var xhr = new XMLHttpRequest();
	  xhr.open('POST', url, true);
	  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	  xhr.onreadystatechange = function() {
	    if (4 == xhr.readyState && 200 == xhr.status) {
			cbk(JSON.parse(xhr.responseText));
		}
	  }
	  xhr.send(buildQueryString(data)); 
    }

    switch (method.toLowerCase()) {
		case 'get': sendGET(url, data, cbk); break;
		case 'post': sendPOST(url, data, cbk); break;
	}
  }
  
  send('POST', '/conn', {imie: 'Marek', wiek: 'mało'}, function(data) {
	  // '?imie=Marek&wiek=malo'
	  console.log(data);	
  });
  


  
  this.start = function() {
    generateBoard(boardWidth, boardHeight);
	initTrace();
	
	document.addEventListener('keydown', function(ev) {
	  switch (ev.which) {
		  case 38: myVec = 'up'; break;
		  case 40: myVec = 'down'; break;
		  case 39: myVec = 'right'; break;
		  case 37: myVec = 'left'; break;
	  }
	  ev.preventDefault();
	});
	
	setTimeout(play, speed);
  }
}

var game = new GraOTron();
game.start();