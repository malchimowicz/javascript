var actions = require('./actions');
var express = require('express');

var app = express();

var reqNo = 0;

app.use(express.static('public'));

app.get('/test.html', function(req, res) {
	console.log('zadanie wyslane');
	res.send('dziala! ' + reqNo);
	reqNo++;
});

app.get('/conn', function(req, res) {
  res.json({msg: 'Witaj ' + req.query.imie});
});

app.listen(3000);