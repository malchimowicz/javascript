var game = {
    worldEl: world,
    heroEl: hero,
    monsterEl: monster
    , init: function () {
        this.worldEl.createWorld(this.heroEl,this.monsterEl);
        this.bindEvent();

        var _this = this;
        var interval = setInterval(function(){
        	if(_this.monsterEl == null || _this.monsterEl == undefined){
        		clearInterval(interval)
        	}
        	var x = _this.monsterEl.posX,
    			y = _this.monsterEl.posY,
    			keyCode = Math.floor(Math.random()*(40-37)) + 37;

            if (keyCode == 37 && x-1>=0 
            	&& !_this.worldEl.checkStone(x-1,y)) {
                _this.monsterEl.posX--;
            }
            else if (keyCode == 38 && y-1>=0
            	&& !_this.worldEl.checkStone(x,y-1)) {
                _this.monsterEl.posY--;
            }
            else if (keyCode == 39 && x+1<_this.worldEl.width
            	&& !_this.worldEl.checkStone(x+1,y)) {
                _this.monsterEl.posX++;
            }
            else if (keyCode == 40 && y+1<_this.worldEl.height
            	&& !_this.worldEl.checkStone(x,y+1)) {
                _this.monsterEl.posY++;
            }
            	
            _this.worldEl.recreateWorld(_this.heroEl,_this.monsterEl);
        },2000)
    }
    , bindEvent: function () {
        var _this = this;
        document.addEventListener('keydown', function (event) {
    		if(_this.heroEl == null && _this.heroEl == undefined){
    			return false;
    		}

    		var x = _this.heroEl.posX,
    			y = _this.heroEl.posY;

            if (event.keyCode == 37 && x-1>=0 
            	&& !_this.worldEl.checkStone(x-1,y)) {
                _this.heroEl.posX--;
            }
            else if (event.keyCode == 38 && y-1>=0
            	&& !_this.worldEl.checkStone(x,y-1)) {
                _this.heroEl.posY--;
            }
            else if (event.keyCode == 39 && x+1<_this.worldEl.width
            	&& !_this.worldEl.checkStone(x+1,y)) {
                _this.heroEl.posX++;
            }
            else if (event.keyCode == 40 && y+1<_this.worldEl.height
            	&& !_this.worldEl.checkStone(x,y+1)) {
                _this.heroEl.posY++;
            }
            	
            if(_this.monsterEl !=null && _this.monsterEl != undefined
            &&_this.heroEl.posX == _this.monsterEl.posX 
            	&& _this.heroEl.posY == _this.monsterEl.posY){
				if(_this.heroEl.equip.indexOf("miecz") !=-1){
					alert("masz miecz!");
					_this.monsterEl = null;
					alert("Pokonałeś!")

				}else{
					alert("nie masz miecza");
					_this.heroEl = null;
					alert("Przegrałeś")
				}
			}






            _this.worldEl.recreateWorld(_this.heroEl,_this.monsterEl);
            console.log(_this.heroEl)
        });
    }
}