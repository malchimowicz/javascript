var world = {
	width:10,
	height:10,
	initWrapper: document.getElementById("init"),
	stone: [{x:3,y:4},{x:6,y:7}],
	items: [
		{x:2,y:6, name:"miecz"}
	],

	createWorld: function(hero){

		var tbl = document.createElement("table");

		for(var i = 0; i < this.height;i++){
			var tr = document.createElement("tr");
		for(var j = 0; j < this.width;j++){
			var td = document.createElement("td");



// postać
			if(hero != null && hero != undefined && hero.posX == j && hero.posY == i){
				td.className += "hero";

			}

			if(monster !=null && monster != undefined && monster.posX == j && monster.posY == i){
				td.className +="monster"
				
			}

			

			if(this.checkStone(j,i)){
				td.className += "stone";

			}
			// przedmiot
			var mapItems = this.getItems(j,i);
			if(mapItems.length > 0){
				for(var z = 0; z < mapItems.length; z++){
					var itemEl = mapItems[z];
					span = document.createElement("span");

					span.className += itemEl.name;

					var _this = this;
					span.addEventListener("click",function(){
						if(hero.posX == itemEl.x && hero.posY == itemEl.y){
						hero.equip.push(itemEl.name);
						delete _this.deleteItem(itemEl);
						 _this.recreateWorld(hero,monster);
						 alert("Masz miecz!")
						}else{
							alert("Jesteś za daleko")
						}
					});
					td.appendChild(span);
				}
			}

		tr.appendChild(td);
		}
		tbl.appendChild(tr);
	}
	this.initWrapper.appendChild(tbl);
	},
	checkStone : function(x,y){
		var stones = this.stone.filter(function(obj){
			 return obj.x == x && obj.y == y;
		});

		return stones.length > 0;
	},

	deleteItem(item){
		var indexItem = this.items.indexOf(item);

		delete this.items[indexItem];
	},



	getItems:function(x,y){
		return this.items.filter(function(obj){
		return obj.x == x && obj.y == y;

	  });
	},
	clearWorld: function(){
		this.initWrapper.innerHTML = "";
	},
	recreateWorld: function(hero){
		this.clearWorld();
		this.createWorld(hero);

	}
}