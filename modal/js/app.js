var contactMeBtn = document.querySelector(".contact-me-btn");
contactMeBtn.addEventListener("click", function() {
  document.body.classList.add("modal-opened");
}, false);

var closeModalBtn = document.querySelector(".modal .close-btn");
closeModalBtn.addEventListener("click", function() {
  document.body.classList.remove("modal-opened");
}, false);

document.addEventListener("keyup", function(evt) {
  if(evt.keyCode === 27){
  document.body.classList.remove("modal-opened");
  }
},false);

var contactForm = document.querySelector(".contact-form");
contactForm.addEventListener("submit",function(etv1){
		etv1.preventDefault();
	 document.body.classList.remove("modal-opened");
},false);

// opcja 2 - zamykanie z setTimeout


// var contactForm = document.querySelector(".contact-form");
// contactForm.addEventListener("submit",function(etv1){
// 		etv1.preventDefault();
// 	window.setTimeout(function(){
//   document.body.classList.remove("modal-opened");
//   },3000);
	
// },false);