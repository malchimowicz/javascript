var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer');

gulp.task("autoprefixer",function(){
	return gulp.src('css/style.css')
	.pipe(autoprefixer())
	.pipe(gulp.dest('dist'));
})